require 'pastel'
require_relative 'string_extend'

class Printer
  def initialize news
    @news = news
  end

  def print
    pastel = Pastel.new
    puts pastel.decorate("#{@news.title} :: #{@news.channel_title}", :bold, :green)
    puts pastel.blue.underline(@news.link)
    puts
    puts pastel.white(@news.description.to_s.remove_html_tags)
    puts
    puts pastel.yellow(@news.date)
    puts "=" * 50
    puts "\n" * 2
  end
end
