require 'open-uri'
require 'feedjira'
require_relative 'news'

class Fetcher
  def self.fetch urls
    news_array = []

    for url in urls
      content = URI.open(url.strip).readlines.join
      feed = Feedjira.parse content
      # binding.pry
      channel_title = feed.title
      for entry in feed.entries
        news = News.new
        news.channel_title = channel_title
        news.title = entry.title
        news.description = entry.summary
        news.link = entry.url
        news.date = entry.published
        news_array << news
      end
    end

    news_array
  end
end