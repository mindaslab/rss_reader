# RSS READER

An RSS reader I wrote because I don't have admin rights to install an RSS reader
in my office laptop, but I badly needed one.

# What you need

Ruby 2.7.1 or some in that range must be installed

# Downloading & Installation

Clone it using git

`$ git clone git@gitlab.com:mindaslab/rss_reader.git`

Goto rss_reader folder and run these commands.

`$ bundle`

# Adding feeds

Open `url.txt` and add or remove feed url, make sure it's one url per line.

# Getting Started

Goto `rss_reader` folder and run this

`$ ruby reader.rb`

or

`$ ruby reader.rb | more` to paginate results.

Enjoy!
