require_relative 'fetcher'
require_relative 'printer'
# require 'pry'

urls = File.open('urls.txt').readlines

news_array = Fetcher.fetch urls
news_array = news_array.sort { |a, b| b.date <=> a.date}

puts
news_array.each do |news|
  Printer.new(news).print
end
